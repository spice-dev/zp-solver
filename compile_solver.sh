#!/bin/bash

source _config/config.sh

mkdir -p lib
mkdir -p bin
mkdir -p build

python build.py

set -o verbose
CPP=$ZP_SOLVER_CPP
UFDIR=$ZP_SOLVER_UFDIR

rm wrapper/*.o

echo $CPP

$CPP -C -g -O3 -xSSE4.2 -axCORE-AVX2 -qopenmp -g -Wall -lstdc++ -I./include -I$UFDIR/UFconfig -I$UFDIR/AMD/Include -I$UFDIR/UMFPACK/Include wrapper/wrapper.c -c -o wrapper/wrapper.o
$CPP -C -g -O3 -xSSE4.2 -axCORE-AVX2 -qopenmp -g -Wall -lstdc++ -I./include -I$UFDIR/UFconfig -I$UFDIR/AMD/Include -I$UFDIR/UMFPACK/Include wrapper/cusewrapper.c -c -o wrapper/cusewrapper.o

