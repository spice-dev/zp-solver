# ZP Solver
This is a solver initially made by Zdenek Pekarek [1] for SPICE3. Solver library was separated into this repository for easier compilation in the future.

## Requirements

 - BLAS
 - UMFPACK library with AMD

## Quick build

 - Copy an example `config.sh` file to `_config/config.sh`.
 - Make necessary changes according to your paths.
 - Run `compile_solver.sh`.
 
## Description

 - `_config\` -- Contains configuration files and examples of those.
 - `autotest\` -- Automatic testing routines.
 - `bin\`, `build\`, `lib\` -- Directory with binary, build and library files (will be created upon compiling).
 - `include\`  -- Header files.
 - `src\` -- All sources.
 - `wrapper\` -- Sources and output for Fortran wrapper files. This is useful for SPICE.

## References
[1] Z. Pekárek. Pokročilé techniky modelování ve fyzice nízkoteplotního a vysokoteplotního plazmatu. PhD thesis. Prague, Czech Republic: Univerzita Karlova, 2012.
