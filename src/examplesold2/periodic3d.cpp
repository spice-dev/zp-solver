/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 64 +1;
  int ny = nx;
  int nz = nx;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(nx*ny*nz, 0.0);
  std::vector<double> LHS(nx*ny*nz, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS = &(LHS[0]);


  //THIS SECTION NEEDS TO BE DONE ONLY ONCE
  //define the type of the solver used
  PoisSolverMultigrid Solver;
  //PoisSolverUMFPACK Solver;
  //set the grid dimensions
  Solver.SetDimensions(nx, ny, nz);
  //Solver.SetPeriodic(true, false, false);
  //Solver.SetPeriodic(false, true, false);
  Solver.SetPeriodic(true, true, false);
  Solver.SetNumLevels(3);
  Solver.SetPostSmooths(20);
  //set the spatial coordinates used for electrode definition
  Solver.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  //print a one line of info for each solve
  Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  //Solver.SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);
  //define the electrodes
  //one small circular in the centre
  Solver.AddSphericalElectrode3D(-0.05, -0.2, 0.0, 0.1,-5.0, true, false);
  Solver.AddSphericalElectrode3D( 0.35, -0.2, 0.0, 0.1, 10.0, true, false);
  Solver.AddSphericalElectrode3D(-0.35,  0.35, 0.0, 0.1, 30.0, true, false);
  Solver.AddSphericalElectrode3D( 0.05,  0.2, 0.0, 0.1,-5.0, true, false);

  //THIS IS REPEATED EVERY PIC STEP
  Solver.Solve(pLHS, pRHS);
  //store the resulting potential
  PoisSolverUtils::DumpVectorToFile(pLHS, "PotentialPeriodic3D", nx, ny, nz);

  return EXIT_SUCCESS;
}


