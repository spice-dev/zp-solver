/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

void SetSolver(PoisSolver *Solver, int nx, int ny)
{
  //set the grid dimensions
  Solver->SetDimensions(nx, ny, 1);
  //set the spatial coordinates used for electrode definition
  Solver->SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  //print a one line of info for each solve
  Solver->SetVerbosityLevel(PoisSolver::VERBOSITY_ONELINER_EACH_SOLVE);
  //define the electrodes
  //one small circular in the centre
  Solver->AddCircularElectrode2D(0.0, 0.0, 0.05, 10.0, true, false);
  Solver->AddCircularElectrode2D(0.0, 0.0, 0.5, 0.0, false, false);
}


int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 800 +1;
  int ny = nx;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(nx*ny, 0.0);
  std::vector<double> LHS1(nx*ny, 0.0);
  std::vector<double> LHS2(nx*ny, 0.0);
  std::vector<double> DIFF(nx*ny, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS1 = &(LHS1[0]);
  double *pLHS2 = &(LHS2[0]);
  double *pDIFF = &(DIFF[0]);

  PoisSolverMultigrid MGSolver;
  SetSolver(&MGSolver, nx, ny);
  MGSolver.Solve(pLHS1, pRHS);

  PoisSolverUMFPACK DirectSolver;
  SetSolver(&DirectSolver, nx, ny);
  DirectSolver.Solve(pLHS2, pRHS);

  //store the results
  PoisSolverUtils::DumpVectorToFile(pLHS1, "PotentialDiffCircularMG", nx, ny, 1);
  PoisSolverUtils::DumpVectorToFile(pLHS2, "PotentialDiffCircularDirect", nx, ny, 1);

  for (int i = 0; i< nx*ny; i++)
    DIFF[i] = LHS1[i]-LHS2[i];
  PoisSolverUtils::DumpVectorToFile(pDIFF, "PotentialDiffCircular", nx, ny, 1);

  return EXIT_SUCCESS;
}


