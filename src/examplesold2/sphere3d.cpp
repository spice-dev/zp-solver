/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 20 +1;
  int ny = nx;
  int nz = nx;
  int ntotal = nx*ny*nz;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(ntotal, 0.0);
  std::vector<double> LHS(ntotal, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS = &(LHS[0]);


  //THIS SECTION NEEDS TO BE DONE ONLY ONCE
  //define the type of the solver used
  //PoisSolverUMFPACK Solver;
  PoisSolverMultigrid Solver;
  //set the grid dimensions
  Solver.SetDimensions(nx, ny, nz);
  Solver.SetNumLevels(2);
  Solver.SetPostSmooths(100);
  //set the spatial coordinates used for electrode definition
  Solver.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  //print a one line of info for each solve
  Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  Solver.SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);//!!!
  //define the electrodes
  //one small circular in the centre
  Solver.AddSphericalElectrode3D(0.0, 0.0, 0.0, 0.2, 10.0, true, false);

  //THIS IS REPEATED EVERY PIC STEP
  Solver.Solve(pLHS, pRHS);
  //store the resulting potential
  PoisSolverUtils::DumpVectorToFile(pLHS, "PotentialSphere3D", nx, ny, nz);

  return EXIT_SUCCESS;
}


