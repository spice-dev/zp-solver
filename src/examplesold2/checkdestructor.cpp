/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 20 +1;
  int ny = nx;
  int nz = nx;
  int ntotal = nx*ny*nz;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(ntotal, 0.0);
  std::vector<double> LHS1(ntotal, 0.0);
  std::vector<double> LHS2(ntotal, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS1 = &(LHS1[0]);
  double *pLHS2 = &(LHS2[0]);

  {
  //THIS SECTION NEEDS TO BE DONE ONLY ONCE
  //define the type of the solver used
  //PoisSolverUMFPACK SolverCoarse;
  PoisSolverMultigrid SolverCoarse;
  //PoisSolverUMFPACK Solver;
  PoisSolverMultigrid Solver;
  //set the grid dimensions
  Solver.SetDimensions(nx, ny, nz);
  SolverCoarse.SetDimensions(nx, ny, nz);
  Solver.SetNumLevels(2);
  Solver.SetPostSmooths(5);
  SolverCoarse.SetNumLevels(2);
  SolverCoarse.SetPostSmooths(5);
  //set the spatial coordinates used for electrode definition
  SolverCoarse.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  Solver.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  //print a one line of info for each solve
  //Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  //Solver.SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);//!!!
  //define the electrodes
  //one small circular in the centre
  SolverCoarse.AddSphericalElectrode3D(0.0, 0.0, 0.0, 0.05, 10.0, true, false);
  Solver.AddSphericalElectrode3DSmooth(0.0, 0.0, 0.0, 0.05, 10.0, true, false);
  //Solver.AddSphericalElectrode3D(0.0, 0.0, 0.0, 0.05, 10.0, true, false);

  //THIS IS REPEATED EVERY PIC STEP
  Solver.Solve(pLHS1, pRHS);
  SolverCoarse.Solve(pLHS2, pRHS);
  //store the resulting potential
  PoisSolverUtils::DumpVectorToFile(pLHS1, "PotentialSphere3DSmooth", nx, ny, nz);
  PoisSolverUtils::DumpVectorToFile(pLHS2, "PotentialSphere3DCoarse", nx, ny, nz);

  std::vector<double> DIFF(ntotal, 0.0);
  for(int i = 0; i< ntotal; i++)
    DIFF[i] = pLHS1[i] - pLHS2[i];

  PoisSolverUtils::DumpVectorToFile(&(DIFF[0]), "PotentialSphere3DDiff", nx, ny, nz);


  }
  printf("Just so there's something after solver destruction\n");

  return EXIT_SUCCESS;
}


