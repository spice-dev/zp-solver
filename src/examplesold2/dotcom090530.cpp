/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

//! Creates a block electrode defined by its extents (both belong to the created block!) with a bias Potential.
//! If OnlyUpdatePotential is true, no new electrode nodes are set, only the existing are set with new bias.
void BlockElectrode3D(PoisSolver &Solver, int x1, int x2, int y1, int y2, int z1, int z2, double Potential, bool OnlyUpdatePotential = false)
{
  printf("Adding a block electrode, x %5d - %5d, y %5d - %5d, z %5d - %5d\n", x1, x2, y1, y2, z1, z2);
  for (int z = z1; z <= z2; z++)
    for (int y = y1; y <= y2; y++)
      for (int x = x1; x <= x2; x++)
        Solver.SetElectrode(Potential, x, y, z, OnlyUpdatePotential);
}


int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 16 +1;//208-104-52-26
  int ny = nx;
  int nz = nx;///2 +1;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(nx*ny*nz, 0.0);
  std::vector<double> LHS(nx*ny*nz, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS = &(LHS[0]);


  //THIS SECTION NEEDS TO BE DONE ONLY ONCE
  //define the type of the solver used
  //PoisSolverMultigrid Solver;
  PoisSolverUMFPACK Solver;
  //set the grid dimensions
  Solver.SetDimensions(nx, ny, nz);
  Solver.SetPeriodic(true, true, false);
  //Solver.SetNumLevels(3);
  //Solver.SetPostSmooths(3);
  //print a one line of info for each solve
  Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  //Solver.SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);

  //int limitx1 = 0;
  //int limitx2 = nx;
  //int limity1 = 22;
  //int limity2 = 42;
  //int limitz = 20;

  double Pot = -3.0;

  //BlockElectrode3D(Solver, 0, nx-1, 0, ny-1, 0, 3, Pot, false);

  //BlockElectrode3D(Solver, 0, nx-1, 0, limity1, 0, limitz, Pot, false);
  //BlockElectrode3D(Solver, 0, nx-1, limity2, ny-1, 0, limitz, Pot, false);
  //BlockElectrode3D(Solver, 0, limitx1, limity2, ny-1, 0, limitz, Pot, false);
  //BlockElectrode3D(Solver, limitx2, nx-1, limity2, ny-1, 0, limitz, Pot, false);

  //set the "floor"
  BlockElectrode3D(Solver, 0, nx-1, 0, ny-1, 0, 3, Pot, false);

  int NSteps = 1;
  double RndAmplitude = 0.0;

  for (int iStep = 0; iStep < NSteps; iStep++)
  {
    //PoisSolverUtils::SetRandom(pRHS, nx, ny, nz, RndAmplitude);
    PoisSolverUtils::FillDensity3D(pRHS, nx, ny, nz, 1.0, 1.0);
    PoisSolverUtils::DumpVectorToFile(pRHS, "DensityBeforeSolve", nx, ny, nz, iStep);
    Solver.Solve(pLHS, pRHS);
    PoisSolverUtils::DumpVectorToFile(pRHS, "DensityPIC", nx, ny, nz, iStep);
    PoisSolverUtils::DumpVectorToFile(pLHS, "PotentialPIC", nx, ny, nz, iStep);
  }

  return EXIT_SUCCESS;
}


