/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

int main( int argc, char *argv[] )
{
  //!!!76 +1 on 2grids works!!!
  //define grid dimensions
  int nx = 40 +1;
  int ny = nx;
  int nz = 1;
  int ntotal = nx*ny*nz;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(ntotal, 0.0);
  std::vector<double> LHS(ntotal, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS = &(LHS[0]);


  //THIS SECTION NEEDS TO BE DONE ONLY ONCE
  //define the type of the solver used
  //PoisSolverUMFPACK Solver;
  PoisSolverMultigrid Solver;
  //set the grid dimensions
  Solver.SetDimensions(nx, ny, nz);
  Solver.SetNumLevels(2);
  Solver.SetPostSmooths(3);
  //set the spatial coordinates used for electrode definition
  Solver.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  //print a one line of info for each solve
  Solver.SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);//!!!
  Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  //define the electrodes
  //one small circular in the centre
  //Solver.AddSPhericalElectrode3D(0.0, 0.0, 0.0, 0.03, 10.0, true, false);
  int CubeHalfLen = 4;
  //Solver.AddRectangElectrodeDiscrete3D(nx/2-CubeHalfLen, nx/2+CubeHalfLen, ny/2-CubeHalfLen, ny/2+CubeHalfLen, nz/2-CubeHalfLen, nz/2+CubeHalfLen, 10.0);
  Solver.AddRectangElectrodeDiscrete2D(nx/2-CubeHalfLen, nx/2+CubeHalfLen, ny/2-CubeHalfLen, ny/2+CubeHalfLen, 10.0);

  //THIS IS REPEATED EVERY PIC STEP
  Solver.Solve(pLHS, pRHS);
  //store the resulting potential
  PoisSolverUtils::DumpVectorToFile(pLHS, "PotentialBrick2D", nx, ny, nz);

  return EXIT_SUCCESS;
}


