/***************************************************************************
*   Copyright (C) 2008 by Zdenek Pekarek                                   *
*   zdenek.pekarek@gmail.com                                               *
***************************************************************************/

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

int main()
{
  int nx = 256 +1;
  int ny = nx;
  int ntotal = nx*ny;

  double *LeftSide1 = new double[ntotal];
  double *LeftSide2 = new double[ntotal];
  double *RightSide = new double[ntotal];

  for (int i = 0; i< ntotal; i++)
    LeftSide1[i] = 0.0;
  for (int i = 0; i< ntotal; i++)
    LeftSide2[i] = 0.0;
  for (int i = 0; i< ntotal; i++)
    RightSide[i] = 0.0;

  PoisSolverMultigrid Solver2;
  //PoisSolverUMFPACK Solver2;

  Solver2.SetDimensions(nx, ny, 1);
  Solver2.SetNumLevels(3);
  Solver2.SetPostSmooths(3);
  Solver2.SetResidualLimit(1e-10);
  Solver2.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  Solver2.AddCircularElectrode2DSmooth(0.0, 0.0, 0.05, 1.0, true, false);
  Solver2.AddCircularElectrode2DSmooth(0.0, 0.0, 0.4, 0.0, false, false);

  Solver2.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  Solver2.Solve(LeftSide2, RightSide);

  //PoisSolverMultigrid Solver;
  PoisSolverUMFPACK Solver;
//
  Solver.SetDimensions(nx, ny, 1);
  Solver.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  Solver.AddCircularElectrode2D(0.0, 0.0, 0.05, 1.0, true, false);
  Solver.AddCircularElectrode2D(0.0, 0.0, 0.4, 0.0, false, false);
//
//  Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_ONELINER_EACH_SOLVE);
//  Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  Solver.Solve(LeftSide1, RightSide);






  PoisSolverUtils::DumpVectorToFile(LeftSide1, "ConcentricCoarse", nx, ny, 1);
  PoisSolverUtils::DumpVectorToFile(LeftSide2, "ConcentricSmooth", nx, ny, 1);

  for (int i = 0; i< ntotal; i++)
    RightSide[i] = LeftSide1[i] - LeftSide2[i];

  PoisSolverUtils::DumpVectorToFile(RightSide, "ConcentricSmoothDifference", nx, ny, 1);

  delete[] LeftSide1;
  delete[] LeftSide2;
  delete[] RightSide;
  return 0;
}
