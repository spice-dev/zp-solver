/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 160 +1;
  int ny = nx;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(nx*ny, 0.0);
  std::vector<double> LHS(nx*ny, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS = &(LHS[0]);

  PoisSolverMultigrid Solver;
  Solver.SetDimensions(nx, ny, 1);
  Solver.SetNumLevels(3);
  //set the spatial coordinates used for electrode definition
  Solver.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  //print a one line of info for each solve
  Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_ONELINER_EACH_SOLVE);
  //define the electrodes
  //one small circular in the centre
  Solver.SetElectrode(10.0, nx/2, ny/2, 0, false);

  int NSteps = 3;
  double Amplitude = 10.0;
  double PI = 3.1415926535;

  for (int iStep = 0; iStep < NSteps; iStep++)
  {
    double Bias = Amplitude*cos(2*PI*iStep/double(NSteps));
  //  Solver.AddCircularElectrode2D(0.0, 0.0, 0.05, Bias, true, true);
    //Solver.SetElectrode(Bias, nx/2, ny/2, 0, true);
    Solver.SetElectrode(Bias, nx/2, ny/2, 0, false);
    Solver.Solve(pLHS, pRHS);
    PoisSolverUtils::DumpVectorToFile(pLHS, "PotentialChangingBias", nx, ny, 1, iStep);
  }

  return EXIT_SUCCESS;
}


