/***************************************************************************
*   Copyright (C) 2008 by Zdenek Pekarek                                   *
*   zdenek.pekarek@gmail.com                                               *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

int main( int argc, char *argv[] )
{
  int nx = 400 +1;
  int ny = nx/2 +1;

  std::vector<double> RHS(nx*ny, 0.0);
  std::vector<double> LHS(nx*ny, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS = &(LHS[0]);

  PoisSolverMultigrid MGSolver;
  //PoisSolverUMFPACK MGSolver;
  MGSolver.SetDimensions(nx, ny, 1);
  MGSolver.SetPeriodic(true, false, false);
  MGSolver.SetNumLevels(3);

  MGSolver.AddRectangElectrodeDiscrete2D(0, nx/3, 0, ny/3, 1.0);
  MGSolver.AddRectangElectrodeDiscrete2D(2*nx/3, nx, 0, ny/3, 1.0);
  MGSolver.AddRectangElectrodeDiscrete2D(0, nx, 0, 0, 1.0);

  //MGSolver.SetPostSmooths(1);
  MGSolver.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  //MGSolver.SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);
  MGSolver.FinishEditing();

  int NSteps = 10;
  double RndAmplitude = 0.001;

  for (int iStep = 0; iStep < NSteps; iStep++)
  {
    double Pot = (20.0*iStep)/NSteps;

    for (int i = 0; i< nx; i++)
    {
      for (int j = 0; j< ny; j++)
      {
        if ((i >= 0) && (i <= nx/3) && (j >= 0) && (j <= ny/3))
          MGSolver.SetElectrode(Pot, i,j,0,true);
      }
    }

    for (int i = 0; i< nx; i++)
    {
      for (int j = 0; j< ny; j++)
      {
        if ((i >= 2*nx/3) && (i <= nx) && (j >= 0) && (j <= ny/3))
          MGSolver.SetElectrode(Pot, i,j,0,true);
      }
    }

    for (int i = 0; i< nx; i++)
    {
      for (int j = 0; j< ny; j++)
      {
        if ((i >= 0) && (i <= nx) && (j >= 0) && (j <= 0))
          MGSolver.SetElectrode(Pot, i,j,0,true);
      }
    }

    //MGSolver.AddRectangElectrodeDiscrete2D(0, nx/3, 0, ny/3, Pot);
    //MGSolver.AddRectangElectrodeDiscrete2D(2*nx/3, nx, 0, ny/3, Pot);
    //MGSolver.AddRectangElectrodeDiscrete2D(0, nx, 0, 0, Pot);


    PoisSolverUtils::SetRandom(pRHS, nx, ny, 1, RndAmplitude);
    MGSolver.Solve(pLHS, pRHS);
    PoisSolverUtils::DumpVectorToFile(pRHS, "DensityPIC-PICDivertor2DX", nx, ny, 1, iStep);
    PoisSolverUtils::DumpVectorToFile(pLHS, "PotentialPIC-PICDivertor2DX", nx, ny, 1, iStep);
  }

  return 0;
}


