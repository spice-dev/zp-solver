#!/usr/bin/python
#import os
import pylab
import numpy
import sys
import math

filenameInput = sys.argv[1]
print "Now processing "+filenameInput

fileInput = open(filenameInput, 'r')
listInput = fileInput.readlines()	
fileInput.close()
	
LineWithDims = listInput[0].split()
x = int(LineWithDims[0])
y = int(LineWithDims[1])
z = int(LineWithDims[2])

print "X x Y x Z", x, y, z
	
Data = []
LineOffset = 1
AbsMin = 1.0e300
AbsMax = -1.0e300
for idxz in range(0,z):	
	A = numpy.zeros([y,x],float)
	Data.append(A)
	for idxy in range(0,y):
		currLine = listInput[y-idxy-1+LineOffset].split()
		for idxx in range(0,x):
			A[idxy, idxx] = float(currLine[idxx])
			if (A[idxy, idxx] < AbsMin):
				AbsMin = A[idxy, idxx]
			if (A[idxy, idxx] > AbsMax):
				AbsMax = A[idxy, idxx]
	LineOffset += y + 1

#if (z <= 1):
	#pylab.imshow(Data[0], origin='lower', interpolation='nearest') #interpolation='bilinear')
#else:
	#Norm = pylab.normalize(vmin=AbsMin, vmax=AbsMax)
	#NSlices = 9
	#Stride = len(Data)/(NSlices+1)
	#for iSlice in range(0,NSlices):
		#pylab.subplot(math.sqrt(NSlices),math.sqrt(NSlices),iSlice+1)
		#idxCurrZ = iSlice*Stride +1
		#pylab.title("z="+str(idxCurrZ))
		#pylab.imshow(Data[idxCurrZ], norm=Norm, origin='lower', interpolation='nearest') #interpolation='bilinear')
		
		
print "min ", AbsMin, " max ", AbsMax, " filename ", filenameInput
if (z <= 1):
	pylab.imshow(Data[0], origin='lower', interpolation='nearest') #interpolation='bilinear')
else:
	Norm = pylab.normalize(vmin=AbsMin, vmax=AbsMax)
	NSubplots1 = math.sqrt(z) + 1
	NSubplots2 = z/NSubplots1 + 1
	for iz in range(0,z):
		pylab.subplot(NSubplots1, NSubplots2,iz +1)
		pylab.title("z="+str(iz))
		pylab.imshow(Data[iz], norm=Norm, origin='lower', interpolation='nearest') #interpolation='bilinear')

pylab.colorbar()
fnameOutput = filenameInput + ".png"
pylab.savefig(fnameOutput, dpi = 300)
