/*
 * Wrapper of the PoisSolver Multigrid C++ interface to C (and Fortran).
 *
 *  Created on: Dec 30, 2008
 *      Author: Zdenek Pekarek
 */

#include "poissolver_multigrid.h"
#define MULTIGRID
//! Call once to alloc and init solver; pointer to it will be returned in the void pointer.
extern "C" void  initsolver_(int *nx, int *ny, int *nz,void **solverpointer);

extern "C" void  tweaksolver_(int *levels, int *smooths, double *res, void **solverpointer);

//! For a given Solver, make it use the Bitmap (0 == plasma, 1 == electrode) and the electrode Bias.
//! If OnlySetNewBias is not 0, no changes in the electrode shape will be performed,
//! only the already existing electrode nodes will get a new bias.
extern "C" void configuresolver_(void **solverpointer, int Bitmap [ ], double Bias [ ], int *OnlySetNewBias,int *nxp,int *nyp, int *nzp);

//! For a given Solver and Density (will be changed during computation) compute the Potential.
extern "C" void solvewithsolver_(void **solverpointer, double Potential [ ], double Density[ ], int *count);

//! Free memory occupied by the Solver.
extern "C" void freesolver_(void **solverpointer);

/////////////////////////////////////////////////////////////////////////////////////////////////////

void  initsolver_(int *nx, int *ny, int *nz, void **solverpointer)
{
printf("Received %d %d %d\n",(int)*nx,(int)*ny,(int)*nz);
//1 means GRID, 2 means UMFPACK
// if((int)*type == 1) {
//  PoisSolverMultigrid *Solver = new PoisSolverMultigrid;
// Solver->SetPostSmooths(20);
// } else {
#ifdef MULTIGRID
printf("Using multigrid solver\n");
  PoisSolverMultigrid *Solver = new PoisSolverMultigrid;
 Solver->SetPostSmooths(5);
 Solver->SetResidualLimit(1e-13);
Solver->SetNumLevels(3);
Solver->SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);

#else
   PoisSolverUMFPACK *Solver = new PoisSolverUMFPACK;

#endif
Solver->SetSummationPeriodicDensity(false);
// }
//Solver->SetNumLeves(2);
Solver->SetDimensions(*nx, *ny, *nz);
  Solver->SetPeriodic(true, true, false);
// Solver->SetPotSmooths(3);
  //comment the next line to improve speed (in later implementations)
  //comment the next line to disable verbose printouts from the solver
//  Solver->SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);
  Solver->SetVerbosityLevel(PoisSolver::VERBOSITY_MUTE);
*solverpointer = Solver;
//  return (void *)Solver;
//printf("Solver pointer %i\n",S);
}

void  tweaksolver_(int *levels, int *smooths,double *res, void **solverpointer)
{
void *Solver;
Solver = *solverpointer;
int lev = (int) *levels;
int smo = (int) *smooths;
double r = (double) *res;

printf("Received: Levels %d, Smooths %d, Residual %e\n",lev,smo,r);

#ifdef MULTIGRID
 PoisSolverMultigrid *TypedSolver = (PoisSolverMultigrid *)Solver;
  TypedSolver->SetPostSmooths(smo);
  TypedSolver->SetResidualLimit(r);
  TypedSolver->SetNumLevels(lev);
#endif
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

void configuresolver_(void **solverpointer, int Bitmap [ ], double Bias [ ], int *OnlySetNewBias, int *nxp,int *nyp, int *nzp)
{
void *Solver;
Solver = *solverpointer;
//printf("Solver pointer %i\n",S);
int nx = (int) *nxp;
int  ny = (int) *nyp;
int nz = (int) *nzp;
#ifdef MULTIGRID
  PoisSolverMultigrid *TypedSolver = (PoisSolverMultigrid *)Solver;
#else
  PoisSolverUMFPACK *TypedSolver = (PoisSolverUMFPACK *)Solver;

#endif
/*  const int nx = TypedSolver->GetNX();
  const int ny = TypedSolver->GetNY();
  const int nz = TypedSolver->GetNZ();
*/
 bool OnlyUpdateBias = ((int) *OnlySetNewBias) ? true : false;
// bool OnlyUpdateBias  = false;
  for(int k = 0; k < nz; k++)
    for(int j = 0; j < ny; j++)
      for(int i = 0; i < nx; i++)
      {
        const int baseidx = i + j*nx + k*nx*ny;
        if (Bitmap[baseidx])
          TypedSolver->SetElectrode(Bias[baseidx], i, j, k,OnlyUpdateBias);
      }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

void solvewithsolver_(void **solverpointer, double Potential [ ], double Density[ ],int *count)
{
void *Solver;
Solver = *solverpointer;
#ifdef MULTIGRID
  PoisSolverMultigrid *TypedSolver = (PoisSolverMultigrid *)Solver;
#else
  PoisSolverUMFPACK *TypedSolver = (PoisSolverUMFPACK *)Solver;

#endif
/*int c = (int) *count;
int nx = 65;
int ny = 65;
int nz = 161;
//if (c%1000 == 1) {
 PoisSolverUtils::DumpVectorToFile(Density, "DensityBeforeSolve", nx, ny, nz,1);*/
//}
  TypedSolver->Solve(Potential, Density);
//if (c%1000 == 1) {

//  PoisSolverUtils::DumpVectorToFile(Potential, "PotentialAfterSolve", nx, ny, nz, 1);
//}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

void freesolver_(void **solverpointer)
{
void *Solver;
Solver = *solverpointer;
#ifdef MULTIGRID
  delete (PoisSolverMultigrid *)Solver;
#else
  delete (PoisSolverUMFPACK *)Solver;

#endif
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
