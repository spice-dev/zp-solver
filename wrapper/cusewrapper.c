/*
Example of the C use of the wrapper to the PoisSolver Multigrid solver. Fortran use should be similar; inspired by
http://publib.boulder.ibm.com/infocenter/macxhelp/v6v81/index.jsp?topic=/com.ibm.xlf81m.doc/pgs/ug68.htm
*/

#include <stdio.h>

extern "C" void * InitSolver(int nx, int ny, int nz);
extern "C" void ConfigureSolver(void *Solver, int *Bitmap, double *Bias, int OnlySetNewBias);
extern "C" void SolveWithSolver(void *Solver, double *Potential, double *Density);
extern "C" void FreeSolver(void *Solver);


/* Prints the contents of the array on the screen */
void PrintArray(double *Arr, int nx, int ny, int nz)
{
  for(int k = 0; k < nz; k++)
  {
    for(int j = 0; j < ny; j++)
    {
      for(int i = 0; i < nx; i++)
      {
        const int baseidx = i + j*nx + k*nx*ny;
        printf("%.2f\t", Arr[baseidx]);
      }
      printf("\n");
    }
  printf("\n");
  }
  printf("-------------------------------------------------\n");
}


int main()
{
  /* these needn't be const, but it's easier to alloc memory in this example */
  const int nx = 8 +1;
  const int ny = 16 +1;
  const int nz = nx;

  int ntotal = nx*ny*nz;

  /* create and init the solver */
  void *Solver = InitSolver(nx, ny, nz);

  double Density[ntotal];
  double Potential[ntotal];
  int    Bitmap[ntotal];
  double Bias[ntotal];

  for (int i = 0; i < ntotal; i++)
  {
    Density[i] = 0.0;
    Potential[i] = 0.0;
    Bitmap[i] = 0;
    Bias[i] = 0.0;
  }

  /* just the simplest possible setting of two planar electrodes on the bottom and on the top  - will work only for 3D */
  const int offsettopmostplane = nx*ny*(nz-1);
  for(int i = 0; i< nx*ny; i++)
  {
    /* set an electrode at z == 0  with a bias of -3 V*/
    Bitmap[i] = 1;
    Bias[i] = -3.0;

    Bitmap[i + offsettopmostplane] = 1;
    Bias[i + offsettopmostplane] = 17.0;
  }

  /* uncomment the following block to see a more interesting potential */
  /*
  int SingleElectrodeNode = nx*ny*nz/2;
  Bitmap[SingleElectrodeNode] = 1;
  Bias[SingleElectrodeNode] = 33.0;
  */

  ConfigureSolver(Solver, Bitmap, Bias, 0);

  SolveWithSolver(Solver, Potential, Density);

  PrintArray(Potential, nx, ny, nz);

  /* destroy the solver */
  FreeSolver(Solver);

  return 0;
}
