#ifndef POISSOLVER_UMFPACK_H_
#define POISSOLVER_UMFPACK_H_

#include "poissolver.h"

//! Nothing interesting to be seen here, just an implementation of PoisSolver direct derived class.
class PoisSolverUMFPACK : public PoisSolverDirect
{
public:
  virtual void Solve(PoisSolverUtils::outerfloat_t *_Potential, PoisSolverUtils::outerfloat_t *_Density);

  PoisSolverUMFPACK();
  ~PoisSolverUMFPACK();

  // ensures cooperation when this class is used as the coarsest level direct solver in multigrids
  friend class PoisSolver_Multigrid;

  virtual bool FinishEditing();

  virtual void Reset();

protected:
  void *umfpack_numeric_;

  long *umfpack_wi_;
  double *umfpack_w_;
  double *umfpack_control_;
  double *umfpack_info_;

  void DestroyDynalloc();

  bool ConstructDecomposition();
};

#endif /*POISSOLVER_UMFPACK_H_*/
