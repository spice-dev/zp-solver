
// to ensure that MUMPS loads correct version of the header file
#ifndef __cplusplus
#define __cplusplus
#endif

#ifndef POISSOLVER_MUMPS_H_
#define POISSOLVER_MUMPS_H_

#include "poissolver.h"
#include "dmumps_c.h"

//! Nothing interesting to be seen here, just an implementation of PoisSolver direct derived class.
class PoisSolverMUMPS : public PoisSolverDirect
{
public:
  virtual void Solve(PoisSolverUtils::outerfloat_t *_Potential, PoisSolverUtils::outerfloat_t *_Density);

  PoisSolverMUMPS();
  ~PoisSolverMUMPS();

  // ensures cooperation when this class is used as the coarsest level direct solver in multigrids
  friend class PoisSolver_Multigrid;

  virtual bool FinishEditing();

  virtual void Reset();

protected:
  DMUMPS_STRUC_C *mumps_par_;

  void DestroyDynalloc();

  bool ConstructDecomposition();
};

#endif /*POISSOLVER_MUMPS_H_*/
