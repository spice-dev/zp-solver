#ifndef POISSOLVER_H_
#define POISSOLVER_H_

#include "poissolver_utils.h"
//class PoisSolverUtils::GridPack;

/* Abstract class defining a common interface of all derived classes.
 */
class PoisSolver
{
public:
//MANDATORY section - methods needed in every computation

  //! Sets the dimensions and allocates basic memory. Use before any other method of the solver.
  //! Dimensions mean the number of grid nodes in each cartesian dimension, thus the number
  //! of grid cells in each direction is one less than the number of nodes.
  //! Currently the number of grid nodes in each dimension must be odd.
  //! Dimensions must be >1 in case of nx and ny, 2D and 3D model is automatically discriminated
  //! using the value of nz (2D for nz = 1).
  //! Returns true on success, false when any problems are encountered (out of memory etc.)
  virtual bool SetDimensions(int nx, int ny, int nz = 1);

  //! Call after altering the electrode bitmap of the solver (through possibly several Add* methods).
  //! Returns true if all operations (including potentially significant allocation of memory for direct solvers)
  //! finish successfully.
  //! Will be called automatically before first actual solving if you forget it.
  virtual bool FinishEditing() =0;

  //! The actual solver subroutine; every time it's called, it computes the potential from the given
  //! charge density (density array might be altered during the computation as well).
  //! Utilizes the previous value of the potential as an initial estimate in methods where applicable.
  virtual void Solve(PoisSolverUtils::outerfloat_t *Potential, PoisSolverUtils::outerfloat_t *Density) =0;

//OPTIONAL section - use these only when needed


  //! True = periodic in that direction, false = potential = const in that diredtion (default). In z the boundary consition is always const potential.
  virtual void SetPeriodic(bool DirX, bool DirY, bool DirZ);

  //! Sets the relative coordinates of grid nodes by defining the coordinates of the central
  //! node and the distance between neighbouring nodes (thus defining a square cell grid).
  //! Needed when using electrode definitions based on spatial coordinates instead of
  //! node index ranges.
  virtual bool SetGeometry(double centerx, double centery, double centerz, double cellstep);

  //! sets the node with 0-indexed coordinates ix, iy, iz as an electrode with Potential. If OnlyUpdatePotential is false,
  //! this might trigger an LU decomposition, if it is true, only the potential and not the status of the node will be changed.
  virtual bool SetElectrode(double Potential, int ix, int iy, int iz, bool OnlyUpdatePotential);

  //! Sets the node with 0-indexed coordinates ix, iy, iz as a free node in plasma, triggers LU decomposition.
  virtual bool UnsetElectrode(int ix, int iy, int iz = 0);

  //! For the node with 0-indexed coordinates ix, iy, iz sets the relative (full length = 1.0) lengths of distances of the node.
  //!!!
  virtual bool SetNextToElectrode(int ix, int iy, int iz, const PoisSolverUtils::Stencil& stencil);

  //! Adds an electrode with Potential as a block of grid nodes between Idx*Low nad Idx*High.
  virtual bool AddRectangElectrodeDiscrete2D(int IdxXLow, int IdxXHigh, int IdxYLow, int IdxYHigh, double Potential);

  //! Self-explaining, no smoothing of edges.
  virtual bool AddCircularElectrode2D(double centerx, double centery, double radius, double potential, bool ElectrodeIsInside, bool OnlyUpdatePotential);

  //! Self-explaining, edges are smoothed.
  virtual bool AddCircularElectrode2DSmooth(double centerx, double centery, double radius, double potential, bool ElectrodeIsInside, bool OnlyUpdatePotential, int LayerZFor3D = 0);

  //! An electrode inside a triplet of vertices, 2 coordinates each.
  virtual bool AddTriangularElectrode2D(double Vrtxs[6], double Potential, bool OnlyUpdatePotential);

  //! A polygon electrode inside Radius segmented into a vector of N elements with Potentials[] each.
  virtual bool AddPolygonElectrode2D(double CenterX, double CenterY, double Radius, const std::vector<double> &Potentials, double Insulator, bool OnlyUpdatePotential);

  //! Self-explaining, no smoothing of edges.
  virtual bool AddSphericalElectrode3D(double centerx, double centery, double centerz, double radius, double potential, bool ElectrodeIsInside, bool OnlyUpdatePotential);

  //! Self-explaining, edges are smoothed.
  virtual bool AddSphericalElectrode3DSmooth(double centerx, double centery, double centerz, double radius, double potential, bool ElectrodeIsInside, bool OnlyUpdatePotential);

  //! Cylindrical electrode with circle in the xz plane (cylinder axis parallel to y).
  virtual bool AddTubularElectrode3DXZSmooth(double centerx, double centerz, double radius, double potential, bool OnlyUpdatePotential, int LayerYFor3D);

  //! Adds an electrode with Potential as a block of grid nodes between Idx*Low nad Idx*High.
  virtual bool AddRectangElectrodeDiscrete3D(int IdxXLow, int IdxXHigh, int IdxYLow, int IdxYHigh, int IdxZLow, int IdxZHigh, double Potential);

  virtual int GetNX() {return grid_pack_[0].GetNX();};
  virtual int GetNY() {return grid_pack_[0].GetNY();};
  virtual int GetNZ() {return grid_pack_[0].GetNZ();};

  //! Possible levels of debug info from none to only during initial phase to detailed during each Solve().
  enum filedump_enum {FILEDUMP_NONE = 0, FILEDUMP_INIT_ONLY, FILEDUMP_DETAILED};
  void SetFileDumpLevel(filedump_enum Level) {level_filedump_ = Level;};

  //! Possible levels of solver verbosity, from no screen printouts to brief initial info to verbose initial info
  //! to one line for each Solve() to verbose for each Solve().
  enum verbosity_enum {VERBOSITY_MUTE = 0, VERBOSITY_BRIEF_INIT, VERBOSITY_ONELINER_EACH_SOLVE, VERBOSITY_VERBOSE_INIT, VERBOSITY_DETAILS_EACH_STEP};
  virtual void SetVerbosityLevel(verbosity_enum Level) {level_verbosity_ = Level;};

  //! Sets the strategy for handling density on periodic boundaries by the solver: true = add the rightmost density to the left most; false = use only leftmost
  //! and erase the rightmost density.
  virtual void SetSummationPeriodicDensity(bool Summation);

  //! Used in multigrids.
  bool AcceptGridPack(PoisSolverUtils::GridPack &GP);

  PoisSolver();
  virtual ~PoisSolver() =0;

protected:

  void PreprocessDensity(PoisSolverUtils::outerfloat_t *Density, bool SumPeriodicDensity) const;

  //possible internal statuses of the solver
  enum status_enum {STATUS_CONSTRUCTED, STATUS_DIMENSIONS_SET, STATUS_READY_FOR_SOLVING, STATUS_ERROR};

  status_enum status_;
  filedump_enum level_filedump_;
  verbosity_enum level_verbosity_;

  std::vector<PoisSolverUtils::GridPack> grid_pack_;

  int num_levels_;
  double posx_[2];
  double posy_[2];
  double posz_[2];

  bool is_3d_;
  bool SummationPeriodicDensity;
};

//! nothing interesting to be seen here, just a derived class to allow all possible direct solvers to share one method of constructing
//! the sparse matrix in the "Yale" format. Besides UMFPACK I can foresee a SuperLU or a PARDISO library based solver.
class PoisSolverDirect : public PoisSolver
{
public:
  PoisSolverDirect();
  ~PoisSolverDirect();
  virtual bool FinishEditing() =0;

protected:
  //vectors representing the sparse matrix
  double *a_;
  PoisSolverUtils::idx_t *asub_;
  PoisSolverUtils::idx_t *xa_;

  size_t nonzeros_;
  size_t nrows_;

  bool ConstructSparseMatrix();

};

#endif /* POISSOLVER_H_ */
