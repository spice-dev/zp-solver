#ifndef POISSOLVER_UTILS_H_
#define POISSOLVER_UTILS_H_

#include <vector>
#include <map>
#include <string>
#include <fstream>
#include <iostream>
#include <sys/time.h>//!!!POSIX

#include <cstdlib>
#include <climits>
#include <cmath>

#ifdef _OPENMP
#include <omp.h>
#endif


//! Nothing to be seen here, auxiliary functions for data structures, debugging or support.
namespace PoisSolverUtils
{

#define HAVE_SINGLE_PRECISION_MG
#ifdef HAVE_SINGLE_PRECISION_MG
typedef float innerfloat_t;
#else
typedef double innerfloat_t;
#endif
typedef double outerfloat_t;


enum stage_t
{
  ERROR = -1, CONSTRUCTED, STALE, UP2DATE
};

static inline double GetWallclockTime()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return double(tv.tv_sec + tv.tv_usec/1000000.0);
}

inline void SetRandom(double *Data, size_t nx, size_t ny, size_t nz = 1, double MaxAmplitude = 1.0);

//! Provides no scaling for cell dimensions.
inline void ComputeIntensity(double *Output, const double *Input, int iDir,
                             size_t nx, size_t ny, size_t nz = 1,
                             bool PeriodicX = false, bool PeriodicY = false, bool PeriodicZ = false);

inline bool FillDensity3D(double *Density, const size_t nx, const size_t ny, const size_t nz, double BottomZVal, double TopZVal);
inline bool FillDensity3DSqr(double *Density, const size_t nx, const size_t ny, const size_t nz, double BottomZVal, double TopZVal);

inline bool FillFromFile(std::vector<double> &Arr, size_t &nx, size_t &ny, size_t &nz, std::string Filename);

inline bool Average3DTo2DOverX(std::vector<double> &Arr2D, const double *Arr3D, int nx, int ny, int nz);

inline bool Distribute2DTo3DOverX(std::vector<double> &Arr3D, const double *Arr2D, int nx, int ny, int nz);



inline double SQR(double x) { return x*x; }

typedef long idx_t; //for UMFPACK
//typedef bool bitmap_t;
//typedef int bitmap_t;
typedef char bitmap_t;

class Stencil
{
protected:
  innerfloat_t xl_, xh_, yl_, yh_, zl_, zh_, center_;
  //innerfloat_t coefs_[6];

  //ugly, isn't it?
  innerfloat_t length0, length1, length2, length3, length4, length5;

  //inline void UpdateCenter(bool Is3D) {center_ = -(xl_ + xh_ + yl_ + yh_); if (Is3D) center_ -= zl_ + zh_;};
  inline void UpdateCenter(bool Is3D) {center_ = -2*(1.0/(length0*length1) + 1.0/(length2*length3)); if (Is3D) center_ = -2*(1.0/(length0*length1) + 1.0/(length2*length3) + 1.0/(length4*length5));};
public:
  void Set(bool Is3D, innerfloat_t coefs[6]);

  inline void SetX(bool Is3D, innerfloat_t minus, innerfloat_t plus) {xl_ = 2.0/(minus*(minus+plus)); xh_ = 2.0/(plus*(minus+plus)); UpdateCenter(Is3D);}
  inline void SetY(bool Is3D, innerfloat_t minus, innerfloat_t plus) {yl_ = 2.0/(minus*(minus+plus)); yh_ = 2.0/(plus*(minus+plus)); UpdateCenter(Is3D);}
  inline void SetZ(bool Is3D, innerfloat_t minus, innerfloat_t plus) {zl_ = 2.0/(minus*(minus+plus)); zh_ = 2.0/(plus*(minus+plus)); UpdateCenter(Is3D);}

  inline innerfloat_t GetXLeft() const  {return xl_;};
  inline innerfloat_t GetXRight() const {return xh_;};
  inline innerfloat_t GetYLeft() const  {return yl_;};
  inline innerfloat_t GetYRight() const {return yh_;};
  inline innerfloat_t GetZLeft() const  {return zl_;};
  inline innerfloat_t GetZRight() const {return zh_;};
  inline innerfloat_t GetCenter() const {return center_;};

  //Implicit means that no shortened edges are in this stencil.
  bool IsImplicit(bool Is3D) const
  {
    bool Answ = true;
    double Eps = 1e-2;
    double Etalon = 4.0;
    if (Is3D)
      Etalon = 6.0;
    //center is < 0
    if (fabs(center_ + Etalon) > Eps)
      Answ = false;
    return Answ;
  }

  //innerfloat_t GetLength(int iDir) const {return coefs_[iDir];};
  innerfloat_t GetLength(int iDir) const
  {
    innerfloat_t Answ;
    if (iDir == 0)
      Answ = length0;
    if (iDir == 1)
      Answ = length1;
    if (iDir == 2)
      Answ = length2;
    if (iDir == 3)
      Answ = length3;
    if (iDir == 4)
      Answ = length4;
    if (iDir == 5)
      Answ = length5;

    return Answ;
  }


  void PrintStencil(bool Is3D) const
  {
    printf("%s lengths %.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t coefs %.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t : %.2f\n", (IsImplicit(Is3D)) ? "impl" : "expl",
    length0, length1, length2, length3, length4, length5,
    xl_, xh_, yl_, yh_, zl_, zh_, center_);
  };

  Stencil(bool Is3D);
  virtual ~Stencil();
  void Reset(bool Is3D);
};

class IndexedStencil : public Stencil
{
public:
  int Idx;
  IndexedStencil(bool Is3D): Stencil(Is3D){};// {/*Idx = i; Reset(Is3D);*/};
};

class GridPack;

struct StencilPack
{
public:
  std::vector<IndexedStencil> NextToElectrodeEven;
  std::vector<IndexedStencil> NextToElectrodeOdd;
  std::vector<IndexedStencil> NextToElectrodeBoth;
  void Clear() {NextToElectrodeEven.clear(); NextToElectrodeOdd.clear(); NextToElectrodeBoth.clear();};
  void PrintStencils (PoisSolverUtils::GridPack* const GP);
};


class GridPack
{
protected:
  stage_t status_;

  int idx_strategy_;

  size_t nx_, ny_, nz_;
  size_t ntotal_;
  size_t offsety_, offsetz_;
  bool periodicx_, periodicy_, periodicz_;
  int verbosity_;

  std::vector<bitmap_t> Bitmap_;         //1...electrode 0...plasma
  std::vector<innerfloat_t> Equipotential_;

  std::map<int, Stencil> MapStencils_;
  std::vector<int> SortedIdxsNextToElectrode_;
  StencilPack StencilsInside;
  StencilPack StencilsPerX;
  StencilPack StencilsPerY;
  StencilPack StencilsPerCornerXY;

public:
  GridPack(int input_verbosity);
  virtual ~GridPack();
  const StencilPack& GetStencilsInside() const {return StencilsInside;};
  const StencilPack& GetStencilsPerX() const {return StencilsPerX;};
  const StencilPack& GetStencilsPerY() const {return StencilsPerY;};
  const StencilPack& GetStencilsPerCornerXY() const {return StencilsPerCornerXY;};

  void SetVerbosityLevel(int input_verbosity) { verbosity_ = input_verbosity; };

  ////////////////////// INIT and EDIT/////////////////////////////////////////
  //! Allocates the memory in all arrays of this grid. Returns true on success.
  virtual bool AllocateAndInit(size_t _nx, size_t _ny, size_t _nz = 1);

  //! Sets/unsets periodicity in direction 0 = x, 1 = y, 2 = z.
  bool SetPeriodicInDirection(int iDir);
  bool UnsetPeriodicInDirection(int iDir, innerfloat_t _Potential1, innerfloat_t _Potential2);

  //! Set a grid node to hold a given potential.
  void SetElectrodeNode(innerfloat_t _Potential, size_t _ix, size_t _iy, size_t _iz = 0);

  //! Set a previously set electrode node to hold a given potential (if no electrode was previously set in that node, this will have no effect).
  void SetEquipotentialAtNode(innerfloat_t _Potential, size_t _ix, size_t _iy, size_t _iz = 0);

  //! Set a grid node to a potential derived from the Poisson equation.
  void UnsetElectrodeNode(size_t _ix, size_t _iy, size_t _iz = 0);

  void SetNextToElectrode(int baseidx, const Stencil& stencil)
  {
    MapStencils_.insert(std::pair<int, Stencil>(baseidx, stencil));
  }

  void FinishEditing(int iLevel);

  void Copy(PoisSolverUtils::GridPack &GP);

  bool RestrictElectrodesFromFiner(GridPack const &FinerGridPack);

  void FillImplicitNextToElectrodes();

  void CleanImplicitNextToElectrodes();

  void ConstructAdditionalNextToElectrodes();

  /////////////////////// ACCESS read-only //////////////////////////////////////
  const bitmap_t     * GetBitmap() const {return &(Bitmap_[0]);}
  //const bitmap_t     * GetNextToElectrode() const {return &(NextToElectrode_[0]);}
  const innerfloat_t * GetEquipotential() const {return &(Equipotential_[0]);}

  inline int GetNX() const {return nx_;}
  inline int GetNY() const {return ny_;}
  inline int GetNZ() const {return nz_;}
  inline int GetNTotal() const {return ntotal_;}

  inline int GetXfromBaseidx(int baseidx) const {return baseidx % GetNX();}
  inline int GetYfromBaseidx(int baseidx) const {return (baseidx %offsetz_)/GetNX();}
  inline int GetZfromBaseidx(int baseidx) const {return baseidx / (offsetz_);}

  bool IsPeriodicX() const { return periodicx_; };
  bool IsPeriodicY() const { return periodicy_; };
  bool IsPeriodicZ() const { return periodicz_; };

  inline bitmap_t IsInsideElectrode(size_t baseidx) const {return Bitmap_[baseidx];}

  //inline bitmap_t IsNextToElectrode(size_t baseidx) const {return NextToElectrode_[baseidx];}
  inline size_t IsNextToElectrode(size_t baseidx) const {return MapStencils_.count(baseidx);}
  inline size_t GetNumNextToElectrode() const {return MapStencils_.size();}

  //! Sets the stencil according to baseidx; (if it's not next to the electrode, just resets it).
  void GetNextToElectrode(size_t baseidx, bool Is3D, Stencil &stencil) const;
  void OverwriteWithElectrodes(float *vec_input) const;
  void OverwriteWithElectrodes(double *vec_input) const;
  void NullifyWithElectrodes(innerfloat_t *vec_input) const;
  void NullifyNextToElectrodes(innerfloat_t *vec_input) const;

  void PrintMapStencils() const
  {
    bool Is3D = (GetNZ() > 1) ? true : false;
    for (std::map<int, Stencil>::const_iterator itMap = MapStencils_.begin(); itMap != MapStencils_.end(); ++itMap)
    {
      int baseidx = itMap->first;
      //printf("Map idx %5d\t", itMap->first);
      printf("Mapidx %5d = %3d x %3d x %5d\t", baseidx, GetXfromBaseidx(baseidx), GetYfromBaseidx(baseidx),GetZfromBaseidx(baseidx));
      (itMap->second).PrintStencil(Is3D);
    }
  }
};




class VectorPack
{
public:
  VectorPack () {};
  virtual ~VectorPack () {};

  virtual bool AllocateAndInit(size_t nx, size_t ny, size_t nz = 1);

  inline innerfloat_t * VectorUpdate() {return &(vec_update_[0]);};
  inline innerfloat_t * VectorResid()  {return &(vec_resid_[0]);};
  inline innerfloat_t * VectorTemp()   {return &(vec_temp_[0]);};

  inline void NullUpdate() { for(size_t i = 0; i < vec_update_.size(); i++) vec_update_[i] = 0.0;};

protected:
  std::vector<innerfloat_t> vec_temp_;
  std::vector<innerfloat_t> vec_update_;
  std::vector<innerfloat_t> vec_resid_;
};

template<class T, class U> static void OverwriteWithElectrodes
  (T *vec, const std::vector<U> &Equipotential, const std::vector<char> &Bitmap, int iStrategy)
{
  //printf("FIX!\n");
  if (iStrategy == 0)
  {
    const int ntotal = int(Bitmap.size());
//OLDER#pragma omp for default(none) shared(vec) schedule(static, 1024)
//#pragma omp parallel if (ntotal > 10000) default(none) shared(vec)
    {
//#pragma omp for schedule(static, 1024)
      for(int baseidx = 0; baseidx < ntotal; baseidx++)
      {
        if (Bitmap[baseidx])
        vec[baseidx] = Equipotential[baseidx];
      }
    }
  }
  else if (iStrategy == 1)
  {
    const int ntotal = int(Bitmap.size());
//#pragma omp for default(none) shared(vec) schedule(static, 1024)
    for(int baseidx = 0; baseidx < ntotal; baseidx++)
      vec[baseidx] = Bitmap[baseidx]*Equipotential[baseidx] + (1-Bitmap[baseidx])*vec[baseidx];
  }

}

template<class T> static bool DumpVectorToFile
    (const T *Data, const char* Label, int nx, int ny, int nz,
    int Counter1 = -1, int Counter2 = -1, int Counter3 = -1, int Precision = 10, bool Scientific = false)
{
  bool Answ = true;
  std::string FileName = Label;
  char Text[100];

  if(Counter1 >=0)
  {
    sprintf(Text, "-%02d", Counter1);
    FileName += Text;
    if(Counter2 >=0)
    {
      sprintf(Text, "-%02d", Counter2);
      FileName += Text;
      if(Counter3 >=0)
      {
        sprintf(Text, "-%02d", Counter3);
        FileName += Text;
      }
    }
  }
  FileName += ".rmt";

  printf("Dumping to file %s\n", FileName.c_str());
  std::ofstream FileStream(FileName.c_str());

  FileStream.setf(std::ios::fixed, std::ios::floatfield);
  if (Scientific)
    FileStream.setf(std::ios::scientific, std::ios::floatfield);

  FileStream.precision(Precision);
  if (!FileStream)
  {
    Answ = false;
  }
  else
  {
    // /*
    FileStream << nx << "\t" << ny << "\t" << nz << "\n";
    for (int k = (nz-1); k >= 0; k--)
    {
      for (int j = (ny-1); j >= 0; j--)
      {
        for (int i = 0; i < nx; i++)
          FileStream << double(Data[i + j*nx +k*nx*ny]) << "\t";
        FileStream << "\n";
      }
      FileStream << "\n";
    }
    //*/
    /*
    FileStream << "ix\tiy\tiz\tvalue\n";
    for (int k = 0; k <nz; k++)
    {
      for (int j = 0; j < ny; j++)
      {
        for (int i = 0; i < nx; i++)
        {
          FileStream << i << "\t" << j << "\t" << k << "\t" <<double(Data[i + j*nx +k*nx*ny]) << "\n";
        }
      }
    }
*/
  }
  return Answ;
}

template<class T> static bool DumpVectorToFile
    (const T *Data, const char* Label, const GridPack& GP,
    int Counter1 = -1, int Counter2 = -1, int Counter3 = -1)
{
  return DumpVectorToFile(Data, Label, GP.GetNX(), GP.GetNY(), GP.GetNZ(), Counter1, Counter2, Counter3);
}

}//end of namespace PoisSolverUtils

//definitions of inlines

inline void PoisSolverUtils::SetRandom(double *Data, size_t nx, size_t ny, size_t nz, double MaxAmplitude)
{
  //double SumSoFar = 0.0;
  for(size_t k = 0; k < nz; k++)
    for(size_t j = 0; j < ny; j++)
      for(size_t i = 0; i < nx; i++)
      {
        double Random = MaxAmplitude*(2.0*(rand()/double(RAND_MAX))-1.0);
        Data[i + j*nx + k*nx*ny] = Random;
        //SumSoFar += fabs(Random);
      }

  //2D edges (3D planes) have only half the density (PIC cell only on one side of the edge)
  //2D corners (3D edges) have only quarter the density
  //3D corners have only 1/8 of the density
  for(size_t k = 0; k < nz; k++)
  {
    for(size_t j = 0; j < ny; j++)
    {
      const int baseidx = j*nx + k*nx*ny;
      Data[baseidx] *= 0.5;
      Data[baseidx + nx-1] *= 0.5;
    }

    for(size_t i = 0; i < nx; i++)
    {
      const int baseidx = i + k*nx*ny;
      Data[baseidx] *= 0.5;
      Data[baseidx + nx*(ny-1)] *= 0.5;
    }
  }
  if (nz > 1)
  {
    for(size_t j = 0; j < ny; j++)
    {
      for(size_t i = 0; i < nx; i++)
      {
        Data[i + j*nx] *= 0.5;
        Data[i + j*nx + nx*ny*(nz-1)] *= 0.5;
      }
    }
  }
}

inline void PoisSolverUtils::ComputeIntensity(double *Output, const double *Input, int iDir,
                             size_t nx, size_t ny, size_t nz,
                             bool PeriodicX, bool PeriodicY, bool PeriodicZ)
{
  size_t ntotal = nx*ny*nz;
  for(size_t i = 0; i < ntotal; i++)
    Output[i] = 0.0;


  if (iDir <= 0 && nx >=3)
  {
    for(size_t k = 0; k < nz; k++)
      for(size_t j = 0; j < ny; j++)
        for(size_t i = 1; i < (nx -1); i++)
        {
          size_t baseidx = i + j*nx + k*nx*ny;
          Output[baseidx] = 0.5*(Input[baseidx+1] - Input[baseidx-1]);
        }

    //!!!handle also borders
  }

  if (iDir == 1 && ny >=3)
  {
    for(size_t k = 0; k < nz; k++)
      for(size_t j = 1; j < (ny-1); j++)
        for(size_t i = 0; i < nx; i++)
        {
          size_t baseidx = i + j*nx + k*nx*ny;
          Output[baseidx] = 0.5*(Input[baseidx+nx] - Input[baseidx-nx]);
        }
    //!!!handle also borders
  }
}

inline bool PoisSolverUtils::FillDensity3D(double *Density, const size_t nx, const size_t ny, const size_t nz, double BottomZVal, double TopZVal)
{
  const size_t ntotal = nx*ny*nz;
  for(size_t i = 0; i < ntotal; i++)
    Density[i] = 0.0;

  if (nz <= 1)
  {
    printf("Invalid use of FillDensity3D!\n");
    return false;
  }

  const double StepZVal = (TopZVal - BottomZVal)/(nz-1);

  for(size_t k = 0; k < nz; k++)
    for(size_t j = 0; j < ny; j++)
      for(size_t i = 0; i < nx; i++)
      {
        const int baseidx = i + j*nx + k*nx*ny;
        Density[baseidx] = BottomZVal + k*StepZVal;
      }

  for(size_t k = 0; k < nz; k++)
  {
    for(size_t j = 0; j < ny; j++)
    {
      const int baseidx = j*nx + k*nx*ny;
      Density[baseidx] *= 0.5;
      Density[baseidx + nx-1] *= 0.5;
    }

    for(size_t i = 0; i < nx; i++)
    {
      const int baseidx = i + k*nx*ny;
      Density[baseidx] *= 0.5;
      Density[baseidx + nx*(ny-1)] *= 0.5;
    }
  }
  if (nz > 1)
  {
    for(size_t j = 0; j < ny; j++)
    {
      for(size_t i = 0; i < nx; i++)
      {
        Density[i + j*nx] *= 0.5;
        Density[i + j*nx + nx*ny*(nz-1)] *= 0.5;
      }
    }
  }

  return true;
}

inline bool PoisSolverUtils::FillDensity3DSqr(double *Density, const size_t nx, const size_t ny, const size_t nz, double BottomZVal, double TopZVal)
{
  const size_t ntotal = nx*ny*nz;
  for(size_t i = 0; i < ntotal; i++)
    Density[i] = 0.0;

  if (nz <= 1)
  {
    printf("Invalid use of FillDensity3DExponent!\n");
    return false;
  }

  //const double StepZVal = (TopZVal - BottomZVal)/(nz-1);

  for(size_t k = 0; k < nz; k++)
    for(size_t j = 0; j < ny; j++)
      for(size_t i = 0; i < nx; i++)
      {
        const int baseidx = i + j*nx + k*nx*ny;
        Density[baseidx] = BottomZVal + (k/(nz-1))*(k/(nz-1))*(TopZVal - BottomZVal);
      }

  for(size_t k = 0; k < nz; k++)
  {
    for(size_t j = 0; j < ny; j++)
    {
      const int baseidx = j*nx + k*nx*ny;
      Density[baseidx] *= 0.5;
      Density[baseidx + nx-1] *= 0.5;
    }

    for(size_t i = 0; i < nx; i++)
    {
      const int baseidx = i + k*nx*ny;
      Density[baseidx] *= 0.5;
      Density[baseidx + nx*(ny-1)] *= 0.5;
    }
  }
  if (nz > 1)
  {
    for(size_t j = 0; j < ny; j++)
    {
      for(size_t i = 0; i < nx; i++)
      {
        Density[i + j*nx] *= 0.5;
        Density[i + j*nx + nx*ny*(nz-1)] *= 0.5;
      }
    }
  }

  return true;
}


inline bool PoisSolverUtils::FillFromFile(std::vector<double> &Arr, size_t &nx, size_t &ny, size_t &nz, std::string Filename)
{
  bool Answ = true;


  std::fstream FileStream (Filename.c_str(), std::fstream::in);
  FileStream >> nx;
  FileStream >> ny;
  FileStream >> nz;

  printf("Loading file %s with dimensions %zu %zu %zu\n", Filename.c_str(), nx, ny, nz);

  Arr.clear();
  Arr.resize(nx*ny*nz);
  for(int k = 0; k < int(nz); k++)
    //for(size_t j = 0; j < ny; j++)
    for(int j = int(ny-1); j >=0; j--)
      for(int i = 0; i < int(nx); i++)
      {
        const int baseidx = i + j*nx + k*nx*ny;
        FileStream >> Arr[baseidx];
      }

  return Answ;
};

inline bool PoisSolverUtils::Average3DTo2DOverX(std::vector<double> &Arr2D, const double *Arr3D, int nx, int ny, int nz)
{
  bool Answ = true;

  //Arr2D.clear();
  //Arr2D.resize(ny*nz, 0.0);

  //sum in yz plane
  for (int k = 0; k < nz; k++)
    for (int j = 0; j < ny; j++)
      for (int i = 0; i < nx; i++)
        Arr2D[j + k*ny] += Arr3D[i + j*nx + k*nx*ny];

  //average in 2D
  for (int k = 0; k < nz; k++)
    for (int j = 0; j < ny; j++)
      Arr2D[j + k*ny] /= nx;

  return Answ;
}

inline bool PoisSolverUtils::Distribute2DTo3DOverX(std::vector<double> &Arr3D, const double *Arr2D, int nx, int ny, int nz)
{
  bool Answ = true;

  //Arr3D.clear();
  //Arr3D.resize(nx*ny*nz, 0.0);

  for (int k = 0; k < nz; k++)
    for (int j = 0; j < ny; j++)
      for (int i = 0; i < nx; i++)
        Arr3D[i+j*nx + k*nx*ny] = Arr2D[j + k*ny];

  return Answ;
}


#endif /* POISSOLVER_UTILS_H_ */
