#ifndef POISSOLVER_MULTIGRID_H_
#define POISSOLVER_MULTIGRID_H_

#include "poissolver.h"
#include "poissolver_umfpack.h"
//class PoisSolverUMFPACK;

class PoisSolverMultigrid : public PoisSolver
{
public:

  //! Set explicitly the number of post-prolongation smoothing full passes (default = 3).
  void SetPostSmooths(int NSmooths) {num_post_smooths_ = NSmooths;};

  //! Set explicitly the limit on local error (default = 1e-7).
  void SetResidualLimit(double ResidualLimit) {limit_residual_ = ResidualLimit;}

  //! Set the number of levels including the level of potential/density and the coarsest level solver (must be >= 2).
  virtual int SetNumLevels(int NumLevels);

  //! Report the number of levels.
  inline int GetNumLevels() const { return num_levels_;};

  //NOT USED YET OR RELISTING OF VIRTUAL FUNCTION DECLARATIONS
  void SetPreSmooths(int NSmooths) {num_pre_smooths_ = NSmooths;};//!!!test
  virtual bool SetNumThreads(int nThreads);
  virtual bool SetDimensions(int nx, int ny, int nz = 1);
  virtual bool FinishEditing();
  static int GetMaxNumLevels(int nx, int ny, int nz = -1);
  virtual void Reset();
  virtual void Solve(PoisSolverUtils::outerfloat_t *_Potential, PoisSolverUtils::outerfloat_t *_Density);
  virtual void SetVerbosityLevel(verbosity_enum Level);

  PoisSolverMultigrid();
  ~PoisSolverMultigrid();

protected:

  PoisSolverUMFPACK coarsest_solver_;
  std::vector<PoisSolverUtils::VectorPack> vector_pack_;

  int num_pre_smooths_;
  int num_post_smooths_;
  double limit_residual_;

  int num_steps_last_solve_;
  double time_last_solve_;

  int num_threads_;

  virtual double ComputeMaxResiduum(const PoisSolverUtils::innerfloat_t *Resid,
                                    const PoisSolverUtils::GridPack &GP,
                                    bool  IsResiduumComputed,
                                    int  *IdxMax = NULL);

  virtual void Restrict2D(PoisSolverUtils::innerfloat_t *Coarse,
                          const PoisSolverUtils::innerfloat_t *Fine,
                          const PoisSolverUtils::GridPack &GPCoarse) const;

  virtual void Restrict3D(PoisSolverUtils::innerfloat_t *Coarse,
                          const PoisSolverUtils::innerfloat_t *Fine,
                          const PoisSolverUtils::GridPack &GPCoarse) const;

  virtual void Prolongate2D(PoisSolverUtils::innerfloat_t *Fine,
                            const PoisSolverUtils::innerfloat_t *Coarse,
                            const PoisSolverUtils::GridPack &GPFine) const;

  virtual void Prolongate3D(PoisSolverUtils::innerfloat_t *Fine,
                            const PoisSolverUtils::innerfloat_t *Coarse,
                            const PoisSolverUtils::GridPack &GPFine) const;

  virtual void SolveCoarsest(PoisSolverUtils::innerfloat_t *U,
                             PoisSolverUtils::innerfloat_t *RHS,
                             int iLevel);
};

#endif /*POISSOLVER_MULTIGRID_H_*/
