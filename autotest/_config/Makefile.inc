# This is a include file for make.
#
# You can either use the file without any changes, or you should specify your own paths.
#
# 	*	If no changes are made, you should compile all external libraries first.
# 	*	If you are using your own libraries, be sure that all of them are compiled with the same compiler (eg. do not mix intelmpi and openmpi etc.). Change only appropriate assignments then.

# standard makefile shell assignment
SHELL=/bin/bash
PICLIB_DIR=/home/podolnik/Coding/spice/spice-lib/_source

FC=ifort
MPIF90=mpiifort

DEBUG_FLAGS=-C -g -r8 -check all -check noarg_temp_created
RELEASE_FLAGS=-DALLOW_NON_INIT -r8 -O3 -axCORE-AVX2 -xSSE4.2 -qopt-report1 -qopt-report-phase=vec -override-limits -qopenmp -fPIC
#RELEASE_FLAGS=$(DEBUG_FLAGS)

FC_FLAGS=-fpp -DPES3D_TESTING -fPIC

CLOCK_LIB=$(PICLIB_DIR)/clock/clock/lib/libclock.a
MEMUSAGE_LIB=$(PICLIB_DIR)/memusage/memusage/lib/libmemusage.a
F2C_LIB=$(PICLIB_DIR)/f2c/f2c/libf2c.a

MATIO_INC=-I$(PICLIB_DIR)/matio/matio-1.3.4/src
MATIO_LIBS=$(PICLIB_DIR)/matio/matio-1.3.4/src/.libs/libmatio.a -lz

MKL_BLAS=-lopenblas
MKL_INC=-I$(MKLROOT)/include/intel64/lp64 -I$(MKLROOT)/include
MKL_LIBS=-Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_core.a $(MKLROOT)/lib/intel64/libmkl_sequential.a $(MKLROOT)/lib/intel64/libmkl_blacs_intelmpi_lp64.a -Wl,--end-group -lpthread -lm -ldl


UMFPACK_LIB=$(PICLIB_DIR)/UF/UMFPACK/Lib/libumfpack.a $(PICLIB_DIR)/UF/AMD/Lib/libamd.a $(MKL_BLAS) $(MKL_LIBS)

ZP_SOLVER_LIB=../../wrapper/wrapper.o ../../lib/libpoissolver.a $(UMFPACK_LIB) -lstdc++

ALL_LIBS=$(CLOCK_LIB) $(MEMUSAGE_LIB) $(MATIO_LIBS) $(F2C_LIB) $(ZP_SOLVER_LIB)
ALL_INC=-I$(PICLIB_DIR)/matio/matio-1.3.4/src $(MKL_INC)

FC_FLAGS_MKL=$(FC_FLAGS) -qopenmp $(ALL_INC)