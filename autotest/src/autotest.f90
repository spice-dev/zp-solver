program pes3dtest
    use getoptions
    use mpi
    use matrix_io3d
    use m3d_tools
    use IFPORT

	implicit none

    character :: okey
    character*2048 :: inputFile, outputFile

    integer*8:: solver_pointer

    integer :: nX, nY, nZ
    real*8 :: dX, dY, dZ
    integer :: n0
    integer, dimension(:,:,:), allocatable :: flagMatrix
    real*8, dimension(:,:,:), allocatable :: potentialMatrix, potentialMatrix2, densityMatrix, densityMatrix2, rand_matrix, pot_mask_m_glob


    integer, dimension(:), allocatable :: vec_i
    real*8, dimension(:), allocatable :: vec_r, vec_p

    integer :: mympi_commsize, mympi_rank, mympi_ierr, mympi_provided_threading, mympi_tag
    integer :: mympi_status(MPI_STATUS_SIZE)


    integer :: elementCount, matrix_dimension
    integer*8 :: time1, time2, time_diff
    real*8 :: init_time

    integer, parameter :: steps = 10
	real*8, dimension(steps) :: stats

    integer :: i, i_x, i_y, i_z, i_r, x, y, z, mg_levels
    real*8 :: one

    real*8, dimension(:,:,:), allocatable :: dum_r
    integer, dimension(:,:,:), allocatable :: dum_i
    integer, dimension(5,6,8) :: dum_ix

    integer*8, dimension(:,:), allocatable :: mem_usage, vm_size
    integer*8, dimension(:), allocatable :: mem_usage_step, vm_size_step

    real*8 :: n_0

    call srand(1234)
    call RANDOM_SEED()

    n_0 = 50

    do
        okey=getopt('o:x:y:z:mn')

        if(okey.eq.'o') then
            outputFile=trim(optarg)
            write(*,*) 'The output will be: ', trim(outputFile)
        end if

        if(okey.eq.'x') then
            read (optarg,*) nX
            write(*,*) 'Size in x: ', nX
        end if

        if(okey.eq.'y') then
            read (optarg,*) nY
            write(*,*) 'Size in y: ', nY
        end if

        if(okey.eq.'z') then
            read (optarg,*) nZ
            write(*,*) 'Size in z: ', nZ
        end if

        if(okey.eq.'>') exit

    enddo

	call MPI_INIT_THREAD(MPI_THREAD_MULTIPLE, mympi_provided_threading, mympi_ierr)
    write(*,*) 'MPI Threaded init', MPI_THREAD_MULTIPLE, mympi_provided_threading

	call MPI_COMM_SIZE(MPI_COMM_WORLD, mympi_commsize, mympi_ierr)
	call MPI_COMM_RANK(MPI_COMM_WORLD, mympi_rank, mympi_ierr)

	allocate(mem_usage(steps + 1, mympi_commsize))
	allocate(mem_usage_step(mympi_commsize))
	allocate(vm_size(steps + 1, mympi_commsize))
	allocate(vm_size_step(mympi_commsize))
	mem_usage = 0
	mem_usage_step = 0
	vm_size = 0
	vm_size_step = 0


    if (mympi_rank == 0) then
        dX = 1
        dY = 1
        dZ = 1
        n0 = 50
    endif

    nX = nX + 1
    nY = nY + 1
    nZ = nZ + 1

    call MPI_BCAST(nX, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(nY, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(nZ, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(dX, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(dY, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(dZ, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(n0, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, mympi_ierr)

    write (*,*) 'Dimensions loaded and distributed.', nX, nY, nZ, dX, dY, dZ, n0
    write (*,*) 'mympi_commsize', mympi_commsize

    matrix_dimension = nX * nY * nZ

    allocate(flagMatrix(nX, nY, nZ))
    allocate(potentialMatrix(nX, nY, nZ))
    allocate(potentialMatrix2(nX, nY, nZ))
    allocate(densityMatrix(nX, nY, nZ))
    allocate(densityMatrix2(nX, nZ, nY))
    allocate(rand_matrix(nX, nZ, nY))
    allocate(vec_r(nX*nZ*nY))
    allocate(vec_p(nX*nZ*nY))
    allocate(vec_i(nX*nZ*nY))

    write (*,*) 'Creating dummy flag matrices'
    call mio3d_createFlag(nX, nY, nZ, flagMatrix)
    call mio3d_createPlasma(nX, nY, nZ, flagMatrix, potentialMatrix, densityMatrix)
    densityMatrix2 = densityMatrix

    mg_levels = 10
    i_x = min(nX, nY, nZ)
    do i_r = 1, mg_levels
        if (i_x <= 32) then
            mg_levels = i_r
            exit
        endif
        i_x = i_x / 2;
    enddo


    call gettime(time1)


    call InitSolver(nX, nY, nZ, solver_pointer)
    call TweakSolver(mg_levels, 2, 1e-13, solver_pointer)
    call m3d_mat2vec_ib(nX, nY, nZ, flagMatrix, vec_i)
    call m3d_mat2vec_r(nX, nY, nZ, potentialMatrix, vec_r)
    call ConfigureSolver(solver_pointer, vec_i, vec_r, 0, nX, nY, nZ)

    call gettime(time2)
    time_diff = time2 - time1
    init_time = 1.0 * time_diff
    write (*,*) '#time_diff;init;', time_diff
    call get_cluster_memory_usage_kb(mem_usage_step, vm_size_step, mympi_commsize)
    mem_usage(1,:) = mem_usage_step(:)
    vm_size(1,:) = vm_size_step(:)

    one = 1
    do i = 1, steps

        call gettime(time1)
        call RANDOM_NUMBER(rand_matrix)
        densityMatrix2 = densityMatrix + (0.0005 * (rand_matrix - 0.5))
        call gettime(time2)
        time_diff = time2 - time1
        write (*,*) '#time_diff;dens;', time_diff


        write (*,*) 'Density sum', sum(densityMatrix2)

        one = - one
        call gettime(time1)

        call m3d_mat2vec_r(nX, nY, nZ, densityMatrix2, vec_r)
        call solvewithsolver(solver_pointer, vec_p, vec_r, i)
        call m3d_vec2mat_r(nX, nY, nZ, vec_p, potentialMatrix2)

        call gettime(time2)
        time_diff = time2 - time1
        write (*,*) '#time_diff;solve;', time_diff
        stats(i) = 1.0 * time_diff

        call get_cluster_memory_usage_kb(mem_usage_step, vm_size_step, mympi_commsize)
        mem_usage(i + 1,:) = mem_usage_step(:)
        vm_size(i + 1,:) = vm_size_step(:)

    enddo



    !write(*,*) potentialMatrix

    call mio3d_saveResults(outputFile, nX, nY, nZ, potentialMatrix2, densityMatrix2, flagMatrix, steps, stats, init_time, mympi_commsize, mem_usage, vm_size)


    write(*,*) 'Stopped 1 '

    deallocate(flagMatrix)
    deallocate(potentialMatrix)
    deallocate(potentialMatrix2)
    deallocate(densityMatrix)
    deallocate(densityMatrix2)
    deallocate(vec_p)
    deallocate(vec_r)
    deallocate(vec_i)


    write(*,*) 'Deallocated 1 '



    deallocate(mem_usage)
    deallocate(mem_usage_step)
    deallocate(vm_size)
    deallocate(vm_size_step)

    write(*,*) 'Deallocated 2'

	call MPI_FINALIZE(mympi_ierr)

end program pes3dtest
