module m3d_tools

    implicit none

contains

    subroutine m3d_rhs(n_x, n_y, n_z, n_0, d_x, d_y, d_z, flagMatrix, potentialMatrix, densityMatrix, rightHandSide)

        implicit none

        ! input properties
        integer, intent(in) :: n_x, n_y, n_z
        real*8, intent(in) :: n_0, d_x, d_z, d_y

        integer, dimension(n_x, n_y, n_z), intent(in):: flagMatrix
        real*8, dimension(n_x, n_y, n_z), intent(in):: potentialMatrix, densityMatrix
        real*8, dimension(n_x * n_y * n_z), intent(out) :: rightHandSide

        integer :: x, y, z, matrixIndex
        real*8 :: charge, chargeQ

        integer*8 :: timeStart, timeEnd, timeDiff

        rightHandSide = 0
        matrixIndex = 1

        !write(*,*) 'sm_rhs', n_z, n_y, d_z, d_y, n0, (real(n0) * d_z * d_y)


        do x = 1, n_x
            do y = 1, n_y
                do z = 1, n_z
                    ! 1 = object; 0 = plasma
                    charge = densityMatrix(x, y, z) / (n_0 * d_z * d_y * d_x)
                    matrixIndex = x + (y - 1)*n_x + (z - 1) * n_y * n_x
                    rightHandSide(matrixIndex) = dble(merge(potentialMatrix(x, y, z), charge, flagMatrix(x, y, z) .ge. 1))
                enddo
            enddo
        enddo
    end subroutine m3d_rhs

    subroutine m3d_mat2vec_r(n_x, n_y, n_z, mat_r, vec_r)

        implicit none

        ! input properties
        integer, intent(in) :: n_x, n_y, n_z
        real*8, dimension(n_x, n_y, n_z), intent(in):: mat_r
        real*8, dimension(n_x * n_y * n_z), intent(out) :: vec_r

        integer :: x, y, z, i

        i = 1

        !do x = 1, n_x
        !    do y = 1, n_y
        !        do z = 1, n_z
        !            i = x + (y - 1)*n_x + (z - 1) * n_y * n_x
        !            vec_r(i) = mat_r(x, y, z)
        !        enddo
        !    enddo
        !enddo
        write(*,*) 'Reshaping'

        vec_r = reshape(mat_r, (/ n_x * n_y * n_z /))
    end subroutine m3d_mat2vec_r

    subroutine m3d_mat2vec_ib(n_x, n_y, n_z, mat_i, vec_ib)

        implicit none

        ! input properties
        integer, intent(in) :: n_x, n_y, n_z
        integer, dimension(n_x, n_y, n_z), intent(in):: mat_i
        integer, dimension(n_x * n_y * n_z), intent(out) :: vec_ib

        integer :: x, y, z, i

        i = 1

        do x = 1, n_x
            do y = 1, n_y
                do z = 1, n_z
                    i = x + (y - 1)*n_x + (z - 1) * n_y * n_x
                    if (mat_i(x, y, z) > 0) then
                        vec_ib(i) = 1
                    else
                        vec_ib(i) = 0
                    endif
                enddo
            enddo
        enddo


    end subroutine m3d_mat2vec_ib



    subroutine m3d_vec2mat_r(n_x, n_y, n_z, vec_r, mat_r)

        implicit none

        ! input properties
        integer, intent(in) :: n_x, n_y, n_z
        real*8, dimension(n_x * n_y * n_z), intent(in) :: vec_r
        real*8, dimension(n_x, n_y, n_z), intent(out):: mat_r

        integer :: x, y, z, i

        !i = 1

        !do x = 1, n_x
        !    do y = 1, n_y
        !        do z = 1, n_z
         !           i = x + (y - 1)*n_x + (z - 1) * n_y * n_x
         !           mat_r(x, y, z) = vec_r(i)
          !      enddo
         !   enddo
        !enddo
        write(*,*) 'Reshaping'
        mat_r = reshape(vec_r, (/ n_x, n_y, n_z /))
    end subroutine m3d_vec2mat_r

end module

