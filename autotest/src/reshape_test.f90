program reshapetest

    implicit none

    integer, parameter :: n_x = 2
    integer, parameter :: n_y = 3
    integer, parameter :: n_z = 4
    real*8, dimension(n_x, n_y, n_z) :: mat_r
    real*8, dimension(n_x*n_y*n_z) :: vec_r

    integer :: i, j, x, y, z

    mat_r = 0
    vec_r = 0

    j = 1
    do x = 1, n_x
        do y = 1, n_y
            do z = 1, n_z
                mat_r(x, y, z) = j;
                j = j + 1;
            enddo
        enddo
    enddo

    call print_mat(n_x, n_y, n_z, mat_r)
    write (*,*) '--'
    call old_mat2vec_r(n_x, n_y, n_z, mat_r, vec_r)
    call print_vec(n_x*n_y*n_z, vec_r)
    write (*,*) '--'

    call new_mat2vec_r(n_x, n_y, n_z, mat_r, vec_r)
    call print_vec(n_x*n_y*n_z, vec_r)
    write (*,*) '--'

    call new_vec2mat_r(n_x, n_y, n_z, vec_r, mat_r)
    call print_mat(n_x, n_y, n_z, mat_r)

end program reshapetest


subroutine new_mat2vec_r(n_x, n_y, n_z, mat_r, vec_r)

    implicit none

    ! input properties
    integer, intent(in) :: n_x, n_y, n_z
    real*8, dimension(n_x, n_y, n_z), intent(in):: mat_r
    real*8, dimension(n_x * n_y * n_z), intent(out) :: vec_r

    integer :: x, y, z, i

    vec_r = reshape(mat_r, (/n_x*n_y*n_z/))
end subroutine new_mat2vec_r


subroutine new_vec2mat_r(n_x, n_y, n_z, vec_r, mat_r)

    implicit none

    ! input properties
    integer, intent(in) :: n_x, n_y, n_z
    real*8, dimension(n_x * n_y * n_z), intent(in) :: vec_r
    real*8, dimension(n_x, n_y, n_z), intent(out):: mat_r

    mat_r = reshape(vec_r, (/n_x, n_y, n_z/))

end subroutine new_vec2mat_r

subroutine print_mat(n_x, n_y, n_z, mat_r)

    integer, intent(in) :: n_x, n_y, n_z
    real*8, dimension(n_x, n_y, n_z), intent(in):: mat_r

    integer :: x, y, z

    do x = 1, n_x
        do y = 1, n_y
            do z = 1, n_z
                write (*,"(1f8.3)",advance="no") mat_r(x, y, z)
            enddo
            write(*,*) ''
        enddo
        write(*,*) ''
    enddo

end subroutine


subroutine print_vec(n, vec_r)

    integer, intent(in) :: n
    real*8, dimension(n), intent(in):: vec_r

    integer :: x

    do x = 1, n
        write (*,"(1f8.3)") vec_r(x)
    enddo

end subroutine

subroutine old_vec2mat_r(n_x, n_y, n_z, vec_r, mat_r)

    implicit none

    ! input properties
    integer, intent(in) :: n_x, n_y, n_z
    real*8, dimension(n_x * n_y * n_z), intent(in) :: vec_r
    real*8, dimension(n_x, n_y, n_z), intent(out):: mat_r

    integer :: x, y, z, i

    i = 1

    do x = 1, n_x
        do y = 1, n_y
            do z = 1, n_z
                i = x + (y - 1)*n_x + (z - 1) * n_y * n_x
                mat_r(x, y, z) = vec_r(i)
            enddo
        enddo
    enddo
end subroutine old_vec2mat_r



subroutine old_mat2vec_r(n_x, n_y, n_z, mat_r, vec_r)

    implicit none

    ! input properties
    integer, intent(in) :: n_x, n_y, n_z
    real*8, dimension(n_x, n_y, n_z), intent(in):: mat_r
    real*8, dimension(n_x * n_y * n_z), intent(out) :: vec_r

    integer :: x, y, z, i

    i = 1

    do x = 1, n_x
        do y = 1, n_y
            do z = 1, n_z
                i = x + (y - 1)*n_x + (z - 1) * n_y * n_x
                vec_r(i) = mat_r(x, y, z)
            enddo
        enddo
    enddo
end subroutine old_mat2vec_r
